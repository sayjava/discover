import {
  GraphQLResolveInfo,
  GraphQLSchema,
  FieldDefinitionNode,
  InputObjectTypeDefinitionNode,
  ObjectTypeDefinitionNode,
  DefinitionNode,
  InputValueDefinitionNode,
} from 'graphql'

/**
 * Enhances a source schema
 * @param config
 */
export function enhance(config: EnhanceConfig): GraphQLSchema

export interface EnhanceConfig {
  /**
   * The schema document to be enhanced
   */
  schema: string

  /**
   * If thiscover directives should be included in the schema
   */
  includeDirectives?: boolean
}

export type AttributeType =
  | 'string'
  | 'number'
  | 'join'
  | 'int'
  | 'boolean'
  | 'ID'

export interface Attribute {
  name: string
  type: AttributeType
  sourceName: string
  isUnique: boolean
}

export interface JoinCondition {
  as: string | Attribute
  by: string | Attribute
}

export interface JoinAttribute extends Attribute {
  isCollection: boolean
  model: string | Model | SelectedModel
  on: JoinCondition
}

export interface ModelSource {
  name: string
  from: string
}

export type ModelAttributes = { [key: string]: Attribute | JoinAttribute }

export interface Model {
  identity: string
  source: ModelSource
  attributes: ModelAttributes
}

export interface SelectedModel extends Model {
  criteria?: Query
}

export type ConditionType =
  | 'EQUAL_TO'
  | 'NOT_EQUAL_TO'
  | 'NOT_IN'
  | 'IN'
  | 'LESS_THAN'
  | 'LESS_THAN_OR_EQUAL'
  | 'GREATER_THAN'
  | 'GREATER_THAN_OR_EQUAL'
  | 'LIKE'

/**
 * Selection conditions
 */
export interface Condition {
  field: string
  value: any
  type: ConditionType
}

type OrderDirection = 'DESC' | 'ASC'

export interface OrderBy {
  field: string
  direction: OrderDirection
}

export interface Query {
  and?: Condition[]
  or?: Condition[]
  limit?: number
  skip?: number
  orderBy?: OrderBy
}

export interface ResolverDatasource {
  find: (selection: SelectedModel) => Promise<Array<any>>
  findOne: (selection: SelectedModel) => Promise<any>
}

export type GraphqlResolver = (
  parent: any,
  args: any,
  context: any,
  info: GraphQLResolveInfo,
) => Promise<any>

export interface Datasources {
  [key: string]: ResolverDatasource
}

export interface Models {
  [key: string]: Model
}

export type ResolverFactory = (adapter: ResolverDatasource) => GraphqlResolver

export interface IntrospectorConfig {
  sources: Datasources
  schema: GraphQLSchema
}

export function introspect(config: IntrospectorConfig): GraphQLSchema

interface DiscoverConfig {
  schema: string
  sources: { [key: string]: Datasource }
}

export function discover(config: DiscoverConfig): GraphQLSchema

export interface DiscoverAPI {
  name: string
  operation: FieldDefinitionNode
  input?: DefinitionNode
}

export type GraphqlObjDef = ObjectTypeDefinitionNode
export type GraphqlFieldDef = FieldDefinitionNode
export type GraphqlInputDef = InputObjectTypeDefinitionNode
export type GraphqlInputValDef = InputValueDefinitionNode

// ---- New API ----- \\
export type DiscoveredAttributeType =
  | 'string'
  | 'number'
  | 'join'
  | 'int'
  | 'boolean'
  | 'ID'

export interface DiscoveredAttribute {
  name: string
  type: DiscoveredAttributeType
  sourceName: string
  unique: boolean
}

export interface DiscoveredRelationCondition {
  as: DiscoveredAttribute
  by: DiscoveredAttribute
}

export interface DiscoveredRelation {
  isCollection: boolean
  model: DiscoveredModel
  type: string
  condition: DiscoveredRelationCondition
}

export interface DiscoveredSource {
  name: string
  from: string
}

export type DiscoveredAttributes = { [key: string]: DiscoveredAttribute }
export type DiscoveredRelations = { [key: string]: DiscoveredRelation }

export interface DiscoveredModel {
  name: string
  source: DiscoveredSource
  attributes: DiscoveredAttributes
  identities: DiscoveredAttribute[]
  relations: DiscoveredRelations
}

export interface Resolvers {
  Query?: {
    [key: string]: GraphqlResolver
  }
  Mutation?: {
    [key: string]: GraphqlResolver
  }
}

export interface ResolverDefinition {
  definition: ObjectTypeDefinitionNode
  models: { [key: string]: DiscoveredModel }
  sources: { [key: string]: Datasource }
}

export interface Criteria {
  and?: Condition[]
  or?: Condition[]
  limit?: number
  skip?: number
  orderBy?: OrderBy
}

export interface FindModel extends DiscoveredModel {
  criteria: Criteria
}

export interface Datasource {
  find: (model: FindModel) => Promise<Array<any>>
}

export interface APIConfig {
  schema: string
  sources: { [key: string]: Datasource }
}
