import { DirectiveDefinitionNode } from 'graphql'

export default (): DirectiveDefinitionNode[] => [
  {
    kind: 'DirectiveDefinition',
    description: undefined,
    name: { kind: 'Name', value: 'unique' },
    repeatable: false,
    locations: [{ kind: 'Name', value: 'FIELD_DEFINITION' }],
  },
  {
    kind: 'DirectiveDefinition',
    description: undefined,
    name: { kind: 'Name', value: 'field' },
    arguments: [
      {
        kind: 'InputValueDefinition',
        name: {
          kind: 'Name',
          value: 'name',
        },
        description: {
          kind: 'StringValue',
          value: 'Remap this field to a different field in the source data',
        },
        type: {
          kind: 'NamedType',
          name: {
            kind: 'Name',
            value: 'String',
          },
        },
      },
    ],
    repeatable: false,
    locations: [{ kind: 'Name', value: 'FIELD_DEFINITION' }],
  },
  {
    kind: 'DirectiveDefinition',
    description: undefined,
    name: { kind: 'Name', value: 'related' },
    arguments: [
      {
        kind: 'InputValueDefinition',
        name: {
          kind: 'Name',
          value: 'by',
        },
        description: {
          kind: 'StringValue',
          value:
            "The name of the field in the child's source data that should be used to connect this relatedship",
        },
        type: {
          kind: 'NamedType',
          name: {
            kind: 'Name',
            value: 'String',
          },
        },
      },
      {
        kind: 'InputValueDefinition',
        name: {
          kind: 'Name',
          value: 'as',
        },
        description: {
          kind: 'StringValue',
          value:
            "The name of the field in the child's source data that should be used to connect this relatedship",
        },
        type: {
          kind: 'NamedType',
          name: {
            kind: 'Name',
            value: 'String',
          },
        },
      },
    ],
    repeatable: false,
    locations: [{ kind: 'Name', value: 'FIELD_DEFINITION' }],
  },
  {
    kind: 'DirectiveDefinition',
    description: undefined,
    name: { kind: 'Name', value: 'source' },
    arguments: [
      {
        kind: 'InputValueDefinition',
        name: {
          kind: 'Name',
          value: 'name',
        },
        description: {
          kind: 'StringValue',
          value: 'The name that this type should referred to in the source',
        },
        type: {
          kind: 'NamedType',
          name: {
            kind: 'Name',
            value: 'String',
          },
        },
      },
      {
        kind: 'InputValueDefinition',
        name: {
          kind: 'Name',
          value: 'from',
        },
        description: {
          kind: 'StringValue',
          value: 'The data source that this type should be source from',
        },
        type: {
          kind: 'NamedType',
          name: {
            kind: 'Name',
            value: 'String',
          },
        },
      },
    ],
    repeatable: false,
    locations: [{ kind: 'Name', value: 'OBJECT' }],
  },
]
