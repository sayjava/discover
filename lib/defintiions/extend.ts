import {
  ObjectTypeDefinitionNode,
  FieldDefinitionNode,
  TypeNode,
} from 'graphql'

import { createNameNode, unwrapType } from '../utils'
import { createFindArgument } from './find'

const isChildList = (typeNode: TypeNode): boolean => {
  switch (typeNode.kind) {
    case 'ListType':
      return true

    case 'NonNullType':
      return isChildList(typeNode.type)
  }

  return false
}

export default (
  objectDef: ObjectTypeDefinitionNode,
): ObjectTypeDefinitionNode => {
  const filterFields: FieldDefinitionNode[] = objectDef.fields
    .filter(field => isChildList(field.type))
    .map(field => {
      const capitalizedName = field.name.value.replace(/^\w/, c =>
        c.toUpperCase(),
      )

      const typeName = unwrapType(field.type)

      return Object.assign({}, field, <FieldDefinitionNode>{
        kind: 'FieldDefinition',
        name: createNameNode(`filter${capitalizedName}`),
        arguments: createFindArgument(typeName),
        type: field.type,
      })
    })

  return Object.assign({}, objectDef, {
    fields: [...objectDef.fields, ...filterFields],
  })
}
