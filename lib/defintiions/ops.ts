import { ObjectTypeDefinitionNode, FieldDefinitionNode } from 'graphql'

export default (
  opsType: string,
  newFields: FieldDefinitionNode[],
  query: ObjectTypeDefinitionNode,
): ObjectTypeDefinitionNode => {
  let newQuery: ObjectTypeDefinitionNode

  if (query) {
    newQuery = Object.assign({}, query, {
      fields: [...query.fields, ...newFields],
    })
  } else {
    newQuery = {
      kind: 'ObjectTypeDefinition',
      description: undefined,
      name: { kind: 'Name', value: opsType },
      fields: newFields,
    }
  }

  return newQuery
}
