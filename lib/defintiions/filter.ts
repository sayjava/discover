import {
  ObjectTypeDefinitionNode,
  InputObjectTypeDefinitionNode,
  InputValueDefinitionNode,
  FieldDefinitionNode,
  TypeNode,
} from 'graphql'
import { unwrapType, isTypeScalar } from '../utils'

/**
 * Given a field definition, create the field filters e.g
 * name_is, name_is_not
 */
// TODO: fix boolean to only accept true or false
const SUFFIXES: { [key: string]: string[] } = {
  Boolean: ['is', 'is_not'],
  Int: ['is', 'is_not', 'lte', 'lt', 'gte', 'gt'],
  Float: ['is', 'is_not', 'lte', 'lt', 'gte', 'gt'],
  String: ['is', 'is_not', 'like', 'in', 'not_in'],
  ID: ['is', 'is_not', 'like', 'in', 'not_in'],
}

const createFieldFilter = (
  field: FieldDefinitionNode,
): InputValueDefinitionNode[] => {
  const fieldType: string = unwrapType(field.type)

  const fieldName = field.name.value

  const typeSuffixes = SUFFIXES[fieldType]

  return typeSuffixes.map(filterSuffix => {
    const returnType: TypeNode =
      filterSuffix.indexOf('in') > -1
        ? {
            kind: 'ListType',
            type: {
              kind: 'NonNullType',
              type: {
                kind: 'NamedType',
                name: {
                  kind: 'Name',
                  value: fieldType,
                },
              },
            },
          }
        : {
            kind: 'NamedType',
            name: {
              kind: 'Name',
              value: fieldType,
            },
          }

    return <InputValueDefinitionNode>{
      kind: 'InputValueDefinition',
      name: {
        kind: 'Name',
        value: `${fieldName}_${filterSuffix}`,
      },
      type: returnType,
    }
  })
}

const combinedFilter = (
  filterName: string,
  combineName,
): InputValueDefinitionNode => {
  return {
    kind: 'InputValueDefinition',
    name: {
      kind: 'Name',
      value: combineName,
    },
    type: {
      kind: 'ListType',
      type: {
        kind: 'NonNullType',
        type: {
          kind: 'NamedType',
          name: {
            kind: 'Name',
            value: filterName,
          },
        },
      },
    },
  }
}

export default (
  typeDef: ObjectTypeDefinitionNode,
): InputObjectTypeDefinitionNode => {
  const inputFields: InputValueDefinitionNode[] = typeDef.fields
    .filter(field => isTypeScalar(field.type))
    .flatMap(createFieldFilter)

  const filterName = `${typeDef.name.value}Filter`

  // include AND and OR filter fields
  const allFields = inputFields.concat([
    combinedFilter(filterName, 'and'),
    combinedFilter(filterName, 'or'),
  ])

  return <InputObjectTypeDefinitionNode>{
    kind: 'InputObjectTypeDefinition',
    name: {
      kind: 'Name',
      value: filterName,
    },
    fields: allFields,
  }
}
