import {
  FieldDefinitionNode,
  NonNullTypeNode,
  ObjectTypeDefinitionNode,
} from 'graphql'
import pluralize from 'pluralize'
import { DiscoverAPI } from '../../index'
import { createDescription, createNamedType, createNameNode } from '../utils'

const createMutationOperation = (
  node: ObjectTypeDefinitionNode,
): FieldDefinitionNode => {
  const { value: nodeName } = node.name
  return {
    kind: 'FieldDefinition',
    name: createNameNode(`update_${nodeName.toLocaleLowerCase()}`),
    // return type of the operation
    type: createNamedType(`_${nodeName}Response`),
    description: createDescription(`Delete ${pluralize(nodeName, 2)}`),
    arguments: [
      {
        kind: 'InputValueDefinition',
        name: createNameNode(`where`),
        type: <NonNullTypeNode>{
          kind: 'NonNullType',
          type: {
            kind: 'NamedType',
            name: createNameNode(`${nodeName}Filter`),
          },
        },
      },
      {
        kind: 'InputValueDefinition',
        name: createNameNode(`set`),
        type: {
          kind: 'NonNullType',
          type: {
            kind: 'NamedType',
            name: createNameNode(`New${nodeName}`),
          },
        },
      },
    ],
  }
}

export default (node: ObjectTypeDefinitionNode): DiscoverAPI => {
  return {
    name: `Create${node.name.value}`,
    operation: createMutationOperation(node),
  }
}
