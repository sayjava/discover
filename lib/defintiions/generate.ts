import {
  DefinitionNode,
  FieldDefinitionNode,
  ObjectTypeDefinitionNode,
} from 'graphql'
import { isJustObjectDefinition, isOperationDefinition } from '../utils'
import createOps from './create'
import deleteOps from './delete'
import createDirectives from './directives'
import createExtension from './extend'
import createInputFilter from './filter'
import findOps from './find'
import createOrUpdateOps from './ops'
import createOrderBy from './orderby'
import createMutationResponse from './response'
import updateOps from './update'

export default (definitions: DefinitionNode[]): DefinitionNode[] => {
  const newDefintions: Array<DefinitionNode> = []
  const mutationFields: Array<FieldDefinitionNode> = []
  const queryFields: Array<FieldDefinitionNode> = []

  definitions
    .filter(isJustObjectDefinition)
    .forEach((defintion: ObjectTypeDefinitionNode) => {
      // Create API  \\
      const createAPI = createOps(defintion)
      newDefintions.push(createAPI.input)
      mutationFields.push(createAPI.operation)

      // General Definitions \\
      const filterInputDefinition = createInputFilter(defintion)
      const orderByInputDefinition = createOrderBy(defintion)
      const mutationResponseDefinition = createMutationResponse(defintion)

      // Delete API \\
      const deleteAPI = deleteOps(defintion)
      mutationFields.push(deleteAPI.operation)

      // Update API \\
      const updateAPI = updateOps(defintion)
      mutationFields.push(updateAPI.operation)

      // Query API \\
      const findAPI = findOps(defintion)
      queryFields.push(findAPI.operation)

      // Extend the basic definition with filter fields \\
      const extendedDefinition = createExtension(defintion)

      // include the original definition
      newDefintions.push(extendedDefinition)

      newDefintions.push(filterInputDefinition)
      newDefintions.push(orderByInputDefinition)
      newDefintions.push(mutationResponseDefinition)
    })

  const ops = <Array<ObjectTypeDefinitionNode>>(
    definitions.filter(isOperationDefinition)
  )

  const [oldMutation] = ops.filter(def => def.name.value === 'Mutation')
  const newMutation = createOrUpdateOps('Mutation', mutationFields, oldMutation)

  const [oldQuery] = ops.filter(def => def.name.value === 'Query')
  const newQuery = createOrUpdateOps('Query', queryFields, oldQuery)

  const directives = createDirectives()

  return [...directives, ...newDefintions, newMutation, newQuery]
}
