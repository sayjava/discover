import { NonNullTypeNode } from 'graphql'
import pluralize from 'pluralize'
import { DiscoverAPI, GraphqlFieldDef, GraphqlObjDef } from '../../index'
import { createDescription, createNamedType, createNameNode } from '../utils'

const createMutationOperation = (node: GraphqlObjDef): GraphqlFieldDef => {
  const { value: nodeName } = node.name
  return {
    kind: 'FieldDefinition',
    name: createNameNode(
      `delete_${pluralize(nodeName.toLocaleLowerCase(), 2)}`,
    ),
    // return type of the operation
    type: createNamedType(`_${nodeName}Response`),
    description: createDescription(`Delete ${pluralize(nodeName, 2)}`),
    arguments: [
      {
        kind: 'InputValueDefinition',
        name: createNameNode(`where`),
        type: <NonNullTypeNode>{
          kind: 'NonNullType',
          type: {
            kind: 'NamedType',
            name: createNameNode(`${nodeName}Filter`),
          },
        },
      },
    ],
  }
}

export default (node: GraphqlObjDef): DiscoverAPI => {
  return {
    name: `Create${node.name.value}`,
    operation: createMutationOperation(node),
  }
}
