import { ListTypeNode, NonNullTypeNode } from 'graphql'
import {
  DiscoverAPI,
  GraphqlFieldDef,
  GraphqlInputDef,
  GraphqlInputValDef,
  GraphqlObjDef,
} from '../../index'
import {
  createDescription,
  createNamedType,
  createNameNode,
  isFieldTypeScalar,
  unwrapType,
} from '../utils'

import pluralize from 'pluralize'

const createScalarFieldInput = (field: GraphqlFieldDef): GraphqlInputValDef => {
  return {
    kind: 'InputValueDefinition',
    name: field.name,
    type: field.type,
  }
}

const createComplextFieldInput = (
  fieldType: string,
  fieldName: string,
): GraphqlInputValDef => {
  return {
    kind: 'InputValueDefinition',
    name: {
      kind: 'Name',
      value: `${fieldName}`,
    },
    type: {
      kind: 'NamedType',
      name: {
        kind: 'Name',
        value: `New${fieldType}`,
      },
    },
    description: {
      kind: 'StringValue',
      value: `Include ${fieldType} in the creation`,
    },
  }
}

const createInputTypeDef = (node: GraphqlObjDef): GraphqlInputDef => {
  return {
    kind: 'InputObjectTypeDefinition',
    name: {
      kind: 'Name',
      value: `New${node.name.value}`,
    },
    description: {
      kind: 'StringValue',
      value: `Create a new ${node.name.value}`,
    },
    fields: node.fields.map(field => {
      const fieldType = unwrapType(field.type)

      if (isFieldTypeScalar(fieldType)) {
        return createScalarFieldInput(field)
      }

      return createComplextFieldInput(fieldType, field.name.value)
    }),
  }
}

// Creates a mutation operation field with return types and arguments
const createMutationOperation = (node: GraphqlObjDef): GraphqlFieldDef => {
  const { value: nodeName } = node.name
  return {
    kind: 'FieldDefinition',
    name: createNameNode(
      `create_${pluralize(nodeName.toLocaleLowerCase(), 2)}`,
    ),
    // return type of the operation
    type: createNamedType(`_${nodeName}Response`),
    description: createDescription(`Create a new ${nodeName}`),
    arguments: [
      {
        kind: 'InputValueDefinition',
        name: createNameNode(`${nodeName.toLocaleLowerCase()}_objects`),
        type: <ListTypeNode>{
          kind: 'ListType',
          type: <NonNullTypeNode>{
            kind: 'NonNullType',
            type: {
              kind: 'NamedType',
              name: createNameNode(`New${nodeName}`),
            },
          },
        },
      },
    ],
  }
}

export default (node: GraphqlObjDef): DiscoverAPI => {
  return {
    name: `Create${node.name.value}`,
    operation: createMutationOperation(node),
    input: createInputTypeDef(node),
  }
}
