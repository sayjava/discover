import {
  ObjectTypeDefinitionNode,
  EnumTypeDefinitionNode,
  EnumValueDefinitionNode,
} from 'graphql'
import { isTypeScalar } from '../utils'

export default (typeDef: ObjectTypeDefinitionNode): EnumTypeDefinitionNode => {
  const values = typeDef.fields
    .filter(field => isTypeScalar(field.type))
    .flatMap(field => {
      return <EnumValueDefinitionNode[]>[
        {
          kind: 'EnumValueDefinition',
          name: {
            kind: 'Name',
            value: `${field.name.value}_DESC`,
          },
        },
        {
          kind: 'EnumValueDefinition',
          name: {
            kind: 'Name',
            value: `${field.name.value}_ASC`,
          },
        },
      ]
    })

  return <EnumTypeDefinitionNode>{
    kind: 'EnumTypeDefinition',
    name: {
      kind: 'Name',
      value: `${typeDef.name.value}OrderBy`,
    },
    values,
  }
}
