import {
  ObjectTypeDefinitionNode,
  FieldDefinitionNode,
  InputValueDefinitionNode,
} from 'graphql'
import { createNameNode } from '../utils'

import pluralize from 'pluralize'
import { DiscoverAPI } from '../../index'

export const createFindArgument = (
  typeName: string,
): InputValueDefinitionNode[] => {
  return [
    {
      kind: 'InputValueDefinition',
      name: createNameNode(`where`),
      type: {
        kind: 'NamedType',
        name: createNameNode(`${typeName}Filter`),
      },
    },
    {
      kind: 'InputValueDefinition',
      name: createNameNode(`orderBy`),
      type: {
        kind: 'NamedType',
        name: createNameNode(`${typeName}OrderBy`),
      },
    },
    {
      kind: 'InputValueDefinition',
      name: createNameNode(`skip`),
      type: {
        kind: 'NamedType',
        name: createNameNode('Int'),
      },
    },
    {
      kind: 'InputValueDefinition',
      name: createNameNode(`limit`),
      type: {
        kind: 'NamedType',
        name: createNameNode('Int'),
      },
    },
  ]
}

export default (objectDef: ObjectTypeDefinitionNode): DiscoverAPI => {
  const typeName = objectDef.name.value

  const operation = <FieldDefinitionNode>{
    kind: 'FieldDefinition',
    name: createNameNode(`find_${pluralize(typeName.toLocaleLowerCase(), 2)}`),
    type: {
      kind: 'ListType',
      type: {
        kind: 'NamedType',
        name: createNameNode(`${typeName}`),
      },
    },
    arguments: createFindArgument(typeName),
  }

  return {
    name: `find_${objectDef.name.value}`,
    operation: operation,
  }
}
