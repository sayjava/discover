import pluralize from 'pluralize'
import { GraphqlFieldDef, GraphqlObjDef } from '../../index'
import { createDescription, createNameNode, createNonNullType } from '../utils'

/**
 * Creating a mutation response from an object defintion,
 *
 * {
 *   totalObjectsChanged
 *   objects
 * }
 */
export default (node: GraphqlObjDef): GraphqlObjDef => {
  const typeName = node.name.value
  const pluralTypeName = pluralize(typeName, 2)

  return {
    kind: 'ObjectTypeDefinition',
    name: createNameNode(`_${node.name.value}Response`),
    description: createDescription(`The response after creating a ${typeName}`),
    fields: <Array<GraphqlFieldDef>>[
      // This is the number objects changed by this mutation operation. i.e created, updated or deleted
      <GraphqlFieldDef>{
        kind: 'FieldDefinition',
        name: createNameNode(`totalRecordsAffected`),
        type: createNonNullType('Int'),
        description: createDescription(
          `The number of ${pluralTypeName} changed`,
        ),
      },
      /**
       * The objects that have changed i.e created, updated, deleted
       */
      <GraphqlFieldDef>{
        kind: 'FieldDefinition',
        name: createNameNode(`${pluralTypeName.toLocaleLowerCase()}`),
        type: {
          kind: 'NonNullType',
          type: {
            kind: 'ListType',
            type: createNonNullType(node.name.value),
          },
        },
      },
    ],
  }
}
