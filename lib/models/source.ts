import { ObjectTypeDefinitionNode } from 'graphql'
import { DiscoveredSource } from '../../index'

import { argsFromDirective } from '../utils'

const SOURCE_DIRECTIVE = 'source'

export default (definition: ObjectTypeDefinitionNode): DiscoveredSource => {
  const { from = 'default', name = definition.name.value } = argsFromDirective(
    definition,
    SOURCE_DIRECTIVE,
  )

  return {
    from,
    name,
  }
}
