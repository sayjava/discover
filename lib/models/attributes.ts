import { FieldDefinitionNode, StringValueNode } from 'graphql'
import { DiscoveredAttribute } from '../../index'

const FIELD_DIRECTIVE = 'field'
const UNIQUE_DIRECTIVE = 'unique'

const RECORD_TYPES: any = {
  Float: 'number',
  Int: 'number',
  String: 'string',
  Boolean: 'boolean',
  ID: 'ID',
}

const getDirectives: any = (field: FieldDefinitionNode): any => {
  return (field.directives || []).reduce((acc, dir) => {
    const dirArgs = {
      [dir.name.value]: dir.arguments.map(
        arg => (arg.value as StringValueNode).value,
      ),
    }

    return Object.assign(acc, dirArgs)
  }, {})
}

export default (
  field: FieldDefinitionNode,
  typeName: string,
): DiscoveredAttribute => {
  const fieldName = field.name.value
  const directives = getDirectives(field)

  const [sourceName] = directives[FIELD_DIRECTIVE] || [fieldName]
  const isUniqueDefined = !!directives[UNIQUE_DIRECTIVE]
  const isUnique = typeName === 'ID' || isUniqueDefined || false

  return {
    name: fieldName,
    type: RECORD_TYPES[typeName],
    sourceName,
    unique: isUnique,
  }
}
