import { FieldDefinitionNode } from 'graphql'
import { DiscoveredRelation, DiscoveredRelationCondition } from '../../index'
import { unwrapType, argsFromDirective } from '../utils'

const RELATION_DIRECTIVE = 'related'

export default (field: FieldDefinitionNode): DiscoveredRelation => {
  const relationCriteria = argsFromDirective(field, RELATION_DIRECTIVE)
  const isCollection = field.type.kind === 'ListType'

  const condition: DiscoveredRelationCondition = Object.assign(
    { by: 'id' },
    relationCriteria,
  )

  const relation: DiscoveredRelation = {
    isCollection,
    condition,
    type: unwrapType(field.type),
    model: null,
  }

  return relation
}
