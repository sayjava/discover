import { FieldDefinitionNode, ObjectTypeDefinitionNode } from 'graphql'
import {
  DiscoveredAttributes,
  DiscoveredModel,
  DiscoveredRelations,
  DiscoveredAttribute,
} from '../../index'
import { GRAPHQL_TYPES, unwrapType } from '../utils'
import createAttributes from './attributes'
import createRelation from './relation'
import createSource from './source'

export const createModel = (
  typedef: ObjectTypeDefinitionNode,
): DiscoveredModel => {
  const attributes: DiscoveredAttributes = {}
  const relations: DiscoveredRelations = {}
  const identities: DiscoveredAttribute[] = []

  typedef.fields.forEach((field: FieldDefinitionNode) => {
    const typeName = unwrapType(field.type)

    if (GRAPHQL_TYPES.includes(typeName)) {
      const attr = createAttributes(field, typeName)
      Object.assign(attributes, {
        [field.name.value]: attr,
      })

      attr.unique && identities.push(attr)
    } else {
      Object.assign(relations, {
        [field.name.value]: createRelation(field),
      })
    }
  })

  // if no identify is defined on the model blowup
  if (identities.length === 0) {
    throw new Error(`No Identity attribute is defined on ${typedef.name.value}`)
  }

  return {
    name: typedef.name.value,
    source: createSource(typedef),
    attributes,
    relations,
    identities,
  }
}

export default createModel
