import { DefinitionNode, ObjectTypeDefinitionNode } from 'graphql'
import { DiscoveredModel } from '../../index'
import { isJustObjectDefinition } from '../utils'
import createModel from './create'

export default (definitions: DefinitionNode[]) => {
  const models: { [key: string]: DiscoveredModel } = {}

  definitions.forEach((typeDef: ObjectTypeDefinitionNode) => {
    const isNotGenerated = !typeDef.name.value.startsWith('_')
    if (isJustObjectDefinition(typeDef) && isNotGenerated) {
      models[typeDef.name.value] = createModel(typeDef)
    }
  })
  return models
}
