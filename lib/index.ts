import { buildASTSchema, parse, printSchema } from 'graphql'
import { addResolveFunctionsToSchema } from 'graphql-tools'
import { APIConfig } from '../index.d'
import generateDefinitions from './defintiions/generate'
import generateModels from './models/generate'
import generateResolvers from './resolvers/generate'

export const createAPI = (config: APIConfig) => {
  const document = parse(config.schema)
  const definitions = document.definitions.concat()

  // generate new definitions
  const newDefintions = generateDefinitions(definitions)

  // generate new models
  const models = generateModels(newDefintions)

  // generate resolvers
  const resolvers = generateResolvers({
    definitions: newDefintions,
    models,
    sources: config.sources,
  })

  // merge the new definitions to a new document
  const newDocument = Object.assign({}, document, {
    definitions: newDefintions,
  })

  // build schema and attache resolvers
  const schema = buildASTSchema(newDocument)

  // console.log(printSchema(schema))

  const executableSchema = addResolveFunctionsToSchema({
    schema,
    resolvers,
  })

  return executableSchema
}
