import { ObjectTypeDefinitionNode } from 'graphql'
import pluralize from 'pluralize'
import { Resolvers } from '../../index'

export default (definition: ObjectTypeDefinitionNode): Resolvers => {
  const typeName = definition.name.value
  const pluralName = pluralize(typeName.toLocaleLowerCase(), 2)

  return {
    Mutation: {
      [`create_${pluralName}`]: () => Promise.resolve({}),
    },
  }
}
