import deepmerge from 'deepmerge'
import { DefinitionNode, ObjectTypeDefinitionNode } from 'graphql'
import { Datasource, DiscoveredModel, ResolverDefinition } from '../../index'
import { isJustObjectDefinition } from '../utils'
import createResolver from './create'
import deleteResolver from './delete'
import findResolver from './find/create'
import updateResolver from './update'

export interface ResolverArgs {
  models: { [key: string]: DiscoveredModel }
  sources: { [key: string]: Datasource }
  definitions: DefinitionNode[]
}

export default (arg: ResolverArgs): any => {
  const { definitions, models, sources } = arg

  const resolvers = definitions
    .filter(isJustObjectDefinition)
    .flatMap((defined: ObjectTypeDefinitionNode) => {
      if (defined.name.value.startsWith('_')) {
        return []
      }

      const resolverDef: ResolverDefinition = {
        sources,
        models,
        definition: defined,
      }

      return [
        createResolver(defined),
        deleteResolver(defined),
        updateResolver(defined),
        findResolver(resolverDef),
      ]
    })

  return deepmerge.all(resolvers)
}
