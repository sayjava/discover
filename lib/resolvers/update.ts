import { ObjectTypeDefinitionNode } from 'graphql'
import { Resolvers } from '../../index'

export default (definition: ObjectTypeDefinitionNode): Resolvers => {
  const typeName = definition.name.value.toLocaleLowerCase()

  return {
    Mutation: {
      [`update_${typeName}`]: () => Promise.resolve({}),
    },
  }
}
