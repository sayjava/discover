import { ArgumentNode } from 'graphql'
import { CriteriaArg } from './index'

export default (criteriaArg: CriteriaArg) => {
  const pageVars = ['skip', 'limit']
  const pagingArgs = criteriaArg.arguments.filter(arg =>
    pageVars.includes(arg.name.value),
  )

  const getValue = (arg: ArgumentNode) => {
    switch (arg.value.kind) {
      case 'Variable':
        return {
          [arg.name.value]: criteriaArg.variables[arg.value.name.value],
        }

      case 'IntValue':
        return {
          [arg.name.value]: Number(arg.value.value),
        }
    }
  }

  return pagingArgs.reduce((values, arg) => {
    return Object.assign(values, getValue(arg))
  }, {})
}
