import { ArgumentNode } from 'graphql'
import { Criteria, DiscoveredModel } from '../../../index'
import createPaging from './paging'
import createOrderby from './orderby'
import createWhere from './where'

export interface CriteriaArg {
  variables: any
  arguments: Readonly<ArgumentNode[]>
  model: DiscoveredModel
}

export default (arg: CriteriaArg): Criteria => {
  return Object.assign(
    {},
    createPaging(arg),
    createOrderby(arg),
    createWhere(arg),
  )
}
