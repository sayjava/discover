import {
  ListValueNode,
  ObjectFieldNode,
  ObjectValueNode,
  StringValueNode,
} from 'graphql'
import { Condition, ConditionType, DiscoveredAttributes } from '../../../index'
import { CriteriaArg } from './index'

const createFilter = (
  fields: Readonly<ObjectFieldNode[]>,
  attributes: DiscoveredAttributes,
  variables: any,
): Condition[] => {
  return fields.map(field => {
    let conditionType: ConditionType

    let fieldValue: any

    const [condition] = field.name.value.match(/(?=_).*/)
    const [fieldName] = field.name.value.split(condition)

    switch (field.value.kind) {
      case 'Variable':
        const variableValue = variables[field.value.name.value] || ''
        if (typeof variableValue === 'number') {
          fieldValue = variableValue
        } else {
          ;[fieldValue] = variableValue.split(condition)
        }
        break

      default:
        fieldValue = (field.value as any).value
        break
    }

    const extractListValues = (argValue: any) => {
      if (argValue.values) {
        return argValue.values.map((v: any) => (v as StringValueNode).value)
      } else {
        return [argValue.value]
      }
    }

    switch (condition) {
      case '_is':
        conditionType = 'EQUAL_TO'
        break

      case '_is_not':
        conditionType = 'NOT_EQUAL_TO'
        break

      case '_not_in':
        conditionType = 'NOT_IN'
        fieldValue = extractListValues(field.value)
        break
      case '_in':
        conditionType = 'IN'
        fieldValue = extractListValues(field.value)
        break

      case '_lte':
        conditionType = 'LESS_THAN_OR_EQUAL'
        break

      case '_lt':
        conditionType = 'LESS_THAN'
        break

      case '_gte':
        conditionType = 'GREATER_THAN_OR_EQUAL'
        break

      case '_gt':
        conditionType = 'GREATER_THAN'
        break

      case '_like':
        conditionType = 'LIKE'
        break

      default:
        conditionType = 'EQUAL_TO'
    }

    return {
      field: `${attributes[fieldName].sourceName}`,
      type: conditionType,
      value: fieldValue,
    }
  })
}

export default (criteriaArg: CriteriaArg) => {
  const { model, arguments: args, variables } = criteriaArg

  const [filterArg] = args.filter(arg => arg.name.value === 'where')

  if (filterArg) {
    const objectValue = <ObjectValueNode>filterArg.value

    const [andFilter] = objectValue.fields.filter(f => f.name.value === 'and')
    const [orFilter] = objectValue.fields.filter(f => f.name.value === 'or')

    const conditionalFilter = andFilter || orFilter

    if (conditionalFilter) {
      // its either a single value or a list
      let conditions: any

      switch (conditionalFilter.value.kind) {
        case 'ListValue':
          let values = (<ListValueNode>conditionalFilter.value).values
          conditions = createFilter(
            values.flatMap(v => (<ObjectValueNode>v).fields),
            model.attributes,
            variables,
          )
          break

        case 'ObjectValue':
          conditions = createFilter(
            (<ObjectValueNode>conditionalFilter.value).fields,
            model.attributes,
            variables,
          )
          break
      }

      if (andFilter) {
        return { and: conditions }
      } else {
        return { or: conditions }
      }
    } else {
      return {
        and: createFilter(objectValue.fields, model.attributes, variables),
      }
    }
  }

  return {}
}
