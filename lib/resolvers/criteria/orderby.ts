import { CriteriaArg } from './index'

export default (criteriaArg: CriteriaArg) => {
  const [orderArg] = criteriaArg.arguments.filter(
    arg => arg.name.value === 'orderBy',
  )

  const splitArg = (arg: string): any => {
    const [field, direction] = arg.split('_')

    return {
      orderBy: {
        field: `${criteriaArg.model.attributes[field]?.sourceName}`,
        direction,
      },
    }
  }

  if (orderArg) {
    switch (orderArg.value.kind) {
      case 'Variable':
        return splitArg(criteriaArg.variables[orderArg.value.name.value])

      case 'EnumValue':
        return splitArg(orderArg.value.value)
    }
  }

  return {}
}
