import { FieldNode, GraphQLResolveInfo } from 'graphql'
import { DiscoveredModel, FindModel, ResolverDefinition } from '../../../index'
import { getModelAndSource } from '../../utils'

import createCriteria from '../criteria/index'

interface Arg {
  resolverDef: ResolverDefinition
  resolverInfo: GraphQLResolveInfo
}

const createSelection = (
  field: FieldNode,
  model: DiscoveredModel,
  models: { [key: string]: DiscoveredModel },
  variables: any,
): FindModel => {
  const attributes = {}
  const relations = {}

  field.selectionSet.selections.forEach((childNode: FieldNode) => {
    const fieldName = childNode.name.value
    const attr = model.attributes[fieldName]
    const relation = model.relations[fieldName]

    if (attr) {
      attributes[fieldName] = Object.assign({}, attr)
    } else {
      // create relationship models
      const childModel = models[relation.type]

      // only include the model if its from same source
      if (childModel.source.from === model.source.from) {
        const relationModel = createSelection(
          childNode,
          models[relation.type],
          models,
          variables,
        )

        relations[fieldName] = Object.assign({}, relation, {
          model: relationModel,
        })
      }
    }
  })

  const criteria = createCriteria({
    arguments: field.arguments,
    variables,
    model,
  })

  return Object.assign({}, model, {
    attributes,
    relations,
    criteria,
  })
}

export default (arg: Arg): FindModel => {
  const { resolverDef, resolverInfo } = arg
  const { source, model } = getModelAndSource(
    arg.resolverDef,
    resolverInfo.returnType,
  )

  const newModel = createSelection(
    resolverInfo.fieldNodes[0],
    model,
    resolverDef.models,
    resolverInfo.variableValues,
  )
  return newModel
}
