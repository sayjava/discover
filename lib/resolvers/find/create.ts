import {
  FieldDefinitionNode,
  GraphQLResolveInfo,
  ObjectTypeDefinitionNode,
} from 'graphql'
import pluralize from 'pluralize'
import { ResolverDefinition, Resolvers, DiscoveredModel } from '../../../index'
import { getModelAndSource, isTypeScalar, unwrapType } from '../../utils'
import createSelection from './selection'

const getRelations = (definition: ObjectTypeDefinitionNode) => {
  return definition.fields.filter(field => !isTypeScalar(field.type))
}

// for each relation field, create a new field resolver
const createRelationResolvers = (
  relations: FieldDefinitionNode[],
  def: ResolverDefinition,
  parent: DiscoveredModel,
): any => {
  return relations.reduce((acc, field) => {
    // Only generate resolves for models from different sources
    const child = def.models[unwrapType(field.type)]

    if (child.source.from !== parent.source.from) {
      return Object.assign(acc, {
        [field.name.value]: createResolver(def),
      })
    }

    return acc
  }, {})
}

const createResolver = (def: ResolverDefinition): any => {
  return async (parent: any, args: any, _: any, info: GraphQLResolveInfo) => {
    // Check Sources are valid \\
    const { model, source } = getModelAndSource(def, info.returnType)

    if (!source) {
      return Promise.reject(
        new Error(
          `Missing datasource called ${model.source.from} for ${model.name}`,
        ),
      )
    }

    // Create find selection
    const findModel = createSelection({
      resolverDef: def,
      resolverInfo: info,
    })

    return source.find(findModel)
  }
}

export default (def: ResolverDefinition): Resolvers => {
  const { definition } = def

  const typeName = definition.name.value
  const pluralName = pluralize(typeName.toLocaleLowerCase(), 2)
  const relations = getRelations(definition)

  const parentModel = def.models[typeName]

  return {
    Query: {
      [`find_${pluralName}`]: createResolver(def),
    },
    [typeName]: createRelationResolvers(relations, def, parentModel),
  }
}
