import {
  DefinitionNode,
  GraphQLOutputType,
  NamedTypeNode,
  NameNode,
  NonNullTypeNode,
  ObjectTypeDefinitionNode,
  StringValueNode,
  TypeNode,
  DirectiveNode,
} from 'graphql'
import { Datasource, DiscoveredModel, ResolverDefinition } from '../index'

export const GRAPHQL_TYPES = [
  'ID',
  'Float',
  'Int',
  'Boolean',
  'String',
  'Query',
  'Mutation',
  '__Schema',
  '__Type',
  '__TypeKind',
  '__Field',
  '__InputValue',
  '__EnumValue',
  '__Directive',
  '__DirectiveLocation',
]

const OPS = ['Mutation', 'Subscription', 'Query']

export const unwrapType = (typeNode: TypeNode): string => {
  switch (typeNode.kind) {
    case 'ListType':
      return unwrapType(typeNode.type)

    case 'NonNullType':
      return unwrapType(typeNode.type)

    case 'NamedType':
      return typeNode.name.value
  }
}

export const isFieldTypeScalar = (fieldTypeName: string) => {
  return ['Int', 'Float', 'ID', 'String', 'Boolean'].includes(fieldTypeName)
}

export const isTypeScalar = (fieldType: TypeNode) => {
  const typeName = unwrapType(fieldType)
  return ['Int', 'Float', 'ID', 'String', 'Boolean'].includes(typeName)
}

export const createNameNode = (name: string): NameNode => ({
  kind: 'Name',
  value: name,
})

export const createNamedType = (type: string): NamedTypeNode => {
  return {
    kind: 'NamedType',
    name: createNameNode(type),
  }
}

export const createNonNullType = (type: string): NonNullTypeNode => {
  return {
    kind: 'NonNullType',
    type: createNamedType(type),
  }
}

export const createDescription = (value: string): StringValueNode => {
  return {
    kind: 'StringValue',
    value,
  }
}

export const isDefinedType = (type: ObjectTypeDefinitionNode): boolean => {
  return GRAPHQL_TYPES.includes(type.name.value)
}

export const isJustObjectDefinition = (def: DefinitionNode): boolean => {
  return def.kind === 'ObjectTypeDefinition' && !OPS.includes(def.name.value)
}

export const isOperationDefinition = (def: DefinitionNode): boolean => {
  return def.kind === 'ObjectTypeDefinition' && OPS.includes(def.name.value)
}

export const getReturnType = (output: GraphQLOutputType) => {
  return output.toString().replace(/!|\[|\]/g, '')
}

export const getModelAndSource = (
  definition: ResolverDefinition,
  output: GraphQLOutputType,
): { model: DiscoveredModel; source: Datasource } => {
  const { models, sources } = definition
  const returnType = getReturnType(output)
  const model = models[returnType]
  const source = sources[model.source.from]

  return { model, source }
}

export const argsFromDirective = (field: any, directiveName: string) => {
  const values: any = (field.directives || [])
    // only joint relationships
    .filter((d: DirectiveNode) => d.name.value === directiveName)
    // map all arguments
    .flatMap((d: DirectiveNode) =>
      d.arguments.map(arg => ({
        name: arg.name.value,
        value: (arg.value as StringValueNode).value,
      })),
    )
    // convert the arguments to a {key1: value1, key2: value2}
    .reduce(
      (acc: any, arg: any) =>
        Object.assign(acc, { [`${arg.name}`]: `${arg.value}` }),
      {},
    )

  return values
}
