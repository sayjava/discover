const withMDX = require("@next/mdx")({
  extension: /\.(md|mdx)$/
});

const withFonts = require("next-fonts");
const withImages = require("next-images");
const withSass = require("@zeit/next-sass");

const isProd = process.env.NODE_ENV === 'production'

module.exports = withMDX(withFonts(
  withSass(
    withImages({
      assetPrefix: isProd ? 'discover' : '',

      pageExtensions: ["js", "jsx", "mdx", "ts", "tsx"],
      typescript: {
        ignoreDevErrors: true
      }
    })
  )
));
