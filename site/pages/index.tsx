export default () => {
  return (
    <>
      <section className="hero is-primary is-large">
        <div className="hero-body">
          <div className="container">
            The one library to rule them all
            {/* <h1 className="title">Medium title</h1>
            <h2 className="subtitle">Medium subtitle</h2> */}
          </div>
        </div>
      </section>
    </>
  );
};
