import { MDXProvider } from '@mdx-js/react'
import { useRouter, NextRouter } from 'next/router'
import Code from '../components/Code'
import Footer from '../components/Footer'

import HomeHero from '../components/HomeHero'
import SmallHero from '../components/SmallHero'

import '../public/static/base.scss'

const components = {
  pre: props => <div {...props} />,
  code: Code,
}

const isHomePage = (router: NextRouter) => router.pathname === '/'
const isDocsPage = (router: NextRouter) => router.pathname.indexOf('docs') > -1

export default ({ Component, pageProps }) => {
  const router = useRouter()
  const isHome = isHomePage(router)
  const isDocs = isDocsPage(router)
  return (
    <MDXProvider components={components}>
      {isHome && <HomeHero />}
      {isDocs && <SmallHero />}

      <main>
        <Component {...pageProps} />
      </main>

      <Footer />
    </MDXProvider>
  )
}
