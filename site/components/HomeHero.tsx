export default () => (
  <section className="home hero is-large">
    <div className="hero-head">
      <header className="navbar">
        <div className="container">
          <div className="navbar-brand">
            <a id="logo" className="navbar-item" href="/">
              Discover
            </a>

            <span
              className="navbar-burger burger"
              data-target="navbarMenuHeroC">
              <span></span>
              <span></span>
              <span></span>
            </span>
          </div>

          <div id="navbarMenuHeroC" className="navbar-menu">
            <div className="navbar-end">
              <a className="navbar-item">Home</a>
              <a className="navbar-item" href="docs">
                Docs
              </a>
              <a className="navbar-item">Examples</a>
              <span className="navbar-item">
                <a className="button is-success is-inverted">
                  <span className="icon">
                    <i className="fab fa-github"></i>
                  </span>
                  <span>Download</span>
                </a>
              </span>
            </div>
          </div>
        </div>
      </header>
    </div>

    <div className="hero-body">
      <div className="container has-text-left">
        <h1 className="main title">Discvoer</h1>
        <h2 className="main subtitle">All new graphql library</h2>
      </div>
    </div>
  </section>
)
