import React from "react";
import Highlight, { defaultProps } from "prism-react-renderer";

const directives = ["@source", "@field", "@related"];

export default ({ children }) => {
  return (
    <Highlight {...defaultProps} code={children} language="graphql">
      {({ className, style, tokens, getLineProps, getTokenProps }) => (
        <pre
          className={className}
          style={{ ...style, padding: "20px", fontSize: "20px" }}
        >
          {tokens.map((line, i) => (
            <div key={i} {...getLineProps({ line, key: i })}>
              {line.map((token, key) => {
                const isKey = directives.includes(token.content);

                const codeProps = getTokenProps({ token, key });

                isKey &&
                  Object.assign(codeProps, {
                    style: {
                      color: "#00ff00"
                    }
                  });

                return <span key={key} {...codeProps} />;
              })}
            </div>
          ))}
        </pre>
      )}
    </Highlight>
  );
};
