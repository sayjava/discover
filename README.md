# Discover

https://github.com/thiscover/discover/workflows/Build/badge.svg
https://github.com/thiscover/discover/workflows/Release/badge.svg

![GitHub Logo](docs/static/overview.svg)

```bash
  npm install @thiscover/discover
  npm install @thiscover/discover-sql-source
```

### Describe Your Data

```graphql
type Movie @source(name: "titles", from: "imdb") {
  id: ID @field(name: "title_id")
  premiered: Int
  title: String @field(name: "primary_title")
}
```

### Attach Your Data Sources

```javascript
import { discover } from '@thiscover/discover'
import { sqlite } from '@thiscover/discover-sql'

const sources: Datasources = {
  imdb: sqlite({ filename: 'imdb.db' }),
}

const executableSchema = discover({ schema, sources })

// ---- Your GraphQL Server Code Here --- \\
```

### Discover Your Data

```graphql
query {
  lastDecade: allMovies(where: { premiered_gte: 2010 }, orderBy: title_DESC) {
    title
    premiered
  }

  avengers: aMovie(where: { title_is: "The Avengers" }) {
    title
    premiered
  }
}
```

## Features

- **Keep your existing schema**
- **You schema is the source of truth**
- **Keep your existing data sources**
- **Pull data from any and multiple data sources**
- **Auto generated query API from the schema**
- **Keep your existing server setup**

[See it in action](https://thiscover-api-sa7zbdkkqq-ew.a.run.app/)

[Schema](/schema.md)
