<!-- docs/_sidebar.md -->

- [Home](/)
- [Schema](/schema.md "Schema")
- [Query](/query.md "The Query API")
- [Datasource](/datasource.md "Datasources")
