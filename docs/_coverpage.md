<!-- _coverpage.md -->

![logo](_media/icon.svg)

# Discover <small>1.0.0</small>

> A Unique GraphQL Library

[GitHub](https://github.com/thiscover/discover)
[Get Started](#Discover)
