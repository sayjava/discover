# Schema

To make automatic discovery of data possible possible, `discover` provides simple directives to help in that process. These directives are can be used when defining your schema to instruct

## Type Uniqueness

Every type defined should ideally have at least a single unique field defined using the graphql native `ID` type. Any field of type `ID` will be automatically treated as unique. The field can be named anything as long as the type is `ID`

```graphql
type Movie {
  title_id: ID
}
```

## Discover Directives

These directives are defined in the `discover` library. They help `discover` to infer meaning and relationships out of a schema to be mapped to a datasource. The following directives can be used when defining a schema :

- [@source](###source) Used for mapping a type to its source in the datasource

- [@field](###field) Useful for mapping field names from datasource to GraphQL

- [@related](###related) Used for defining relationships between types

### @source

This directive allows a GraphQL type to be mapped to a datasource and it's name in tha source

| Parameter | Description                                                                                                  | Default                                                                                         |
| :-------- | :----------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------- |
| **name**  | The name of this type in the datasource. Eg like a table name in an SQL or a collection in a MongoDB         | It defaults to the name of the type in the schema                                               |
| **from**  | The datasource that this type should be sourced from. discover can source types from different data sources. | It defaults to ‘default’. A default data source should be configured when initialising discover |

```graphql
type Movie @source(name: "titles", from: "imdb") {}
```

### @field

This field is useful for remapping field names in legacy systems or fields that are named

| Parameter | Description                                                                                                                                                  | Default                                             |
| :-------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------- |
| **name**  | The name of this field in the datasource if it is different than what is defined in the schema. This is useful for remapping names from legacy data sources. | If not defined on a field, it uses the schema name. |

```graphql
type Movie @source(name: "titles", from: "imdb") {
  # title_id mapped to id
  id: ID @field(name: "title_id")

  # primary_title  mapped to title
  title: String @field(name: "primary_title")

  # runtime_minutes mapped to runtime
  runtime: Int @field(name: "runtime_minutes")

  # fields with no directive are left as is
  premiered: Int
}
```

### @related

A relation is defined between two types. This directive dictates how discover should discover the relationship between types. A relationship must be referenced both ways.

| Parameter | Description                                                            | Default                                                                                                              |
| :-------- | :--------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------- |
| **by**    | The field by which this relationship is established                    | Uses `id` as a fallback                                                                                              |
| **as**    | The field in the other type on which this relationship is established. | discover will look for the first type in the other counterpart that correlates to the type defining the relationship |

#### One-to-One

- A `Movie` has a `rating`
- A `Rating` belongs to a `movie`.

The movie references a rating and the rating references the movie. Both types have to reference each other for the connection to work.

```graphql
type Movie @source(name: "titles", from: "imdb") {
  id: ID @field(name: "title_id")
  title: String @field(name: "primary_title")

  # A movie is related by `title_id` to a rating
  # and referenced as `movie` in a rating.
  # the `by` parameter is ommitted here,
  # because the `id` (mapped to title_id) is automatically used
  rating: Rating @related(as: "movie")
}

type Rating @source(name: "ratings", from: "imdb") {
  rating: String

  # A rating is related to a movie
  movie: Movie @related(by: "title_id")
}
```

Using a SQL source, a `rating` table will have a `title_id` column.

#### One-to-Many

- A `Person` has been `casted` many times as a `Crew` member in a movie
- A `crew` is a person

```graphql
type Crew @source(name: "crew", from: "imdb") {
  crew_id: ID
  category: String
  job: String

  # A crew memember references back to a person
  # in the one-to-one relationship back to a person
  person: Person @related(by: "person_id")
}

type Person @source(name: "people", from: "sqlite") {
  id: ID @field(name: "person_id")
  name: String

  # A person has been casted many times
  # the `as` parameter is ommitted, and `discover`
  # will use the `Person` type in the creq to infer the
  # referenced relationship
  casted: [Crew] @related(by: "person_id")
}
```

E.g using a SQL source, a `crew` table will have `title_id` and `person_id` columns

#### Many-to-Many

- A `Movie` has many `crew` members
- A `Crew` member is part of many `movies`

```graphql
type Movie @source(name: "titles", from: "imdb") {
  id: ID @field(name: "title_id")
  title: String @field(name: "primary_title")

  # Moive is referenced as `movies` from a crew.
  # This will represent how many movies they have worked on.
  crews: [Crew] @related(as: "movies")
}

type Crew @source(name: "crew", from: "imdb") {
  crew_id: ID
  category: String
  job: String

  # A crew has many movies under their belt.
  movies: [Movie] @related(by: "title_id")
}
```

E.g using a SQL source, a `crew` table will have `title_id` and `person_id` columns.

## Multiple Datasources

- A `Movie` is sourced from a sqlite database called `imdb`
- A `Mapping` is sourced from a csv file that maps imdb `imdb_id` (named as title_id) to `itunes_id` (named as track_id)
- An `ItunesMovie` represents a Movie entity from the itunes store. It sources the data directly from iTunes using the public API

```graphql
type Movie @source(name: "titles", from: "imdb") {
  id: ID @field(name: "title_id")
  title: String @field(name: "primary_title")

  extras: Mapping @related(as: "movie")
}

# Type is sourced from imdbmap
type Mapping @source(from: "imdbmap") {
  # represents an IMDB movie
  movie: Movie @related(by: "imdb_id")

  # represents an ITunes movie record
  itunes: ItunesMovie @related(by: "itunes_id")
}

# Type is sourced from itunes using their http api
type ItunesMovie @source(from: "itunes") {
  id: ID @field(name: "trackId")

  name: String @field(name: "trackName")
  price: Float @field(name: "trackHdPrice")

  mapped: Mapping @related(as: "itunes")
}
```

Thus, a movie's price can be discovered as follows:

```graphql
# What is the current price of the movie called "Tron" on the iTunes store.
query {
  aMovie(where: { title_is: "Tron" }) {
    title

    extras {
      itunes {
        price
      }
    }
  }
}
```
