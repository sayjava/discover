# Datasources

## API

The datasource api is

```javascript
   DatasourceResolver interface {
       /**
        * Called
        * */
        find: (selection: SelectedModel) => Promise<any>,

        /**
         * Called
         * */
        findOne: (selection: SelectedModel) => Promise<any>
    }
```

## SQL

### SQLite3

```javascript
console.log("coming soon");
```

### MySQL

```javascript
console.log("coming soon");
```

### Postgres

```javascript
console.log("coming soon");
```

## Filesystem

```javascript
console.log("coming soon");
```

### CSV

```javascript
console.log("coming soon");
```

### Excel

```javascript
console.log("coming soon");
```
