# Query API

Seet it in action at [IMDB](https://thiscover-api-sa7zbdkkqq-ew.a.run.app/) Graphql API

## Single Type fetches

--single fetches--

**Find a single person in the imdb database**

```graphql
query {
  aPerson(where: { id_is: "nm0000102" }) {
    name
  }
}
```

## Multi type queries

**Find everyone but limit to just 10 people**

```graphql
query {
  allPeople(limit: 10) {
    name
  }
}
```

**Find everyone who is a composer**

```graphql
query {
  allCrews(where: { category_is: "composer" }) {
    category
  }
}
```

## Relationships

-- filter queries ---

--simple relationships--

**Find everyone who acted in a movie and their role**

```graphql
query {
  allMovies(limit: 1) {
    originalTitle
    crews {
      category
      person {
        name
      }
    }
  }
}
```

**Find everyone who composed for this movie**

```graphql
query {
  allMovies(limit: 1) {
    originalTitle
    filterCrews(where: { category_is: "composer" }) {
      category
      person {
        name
      }
    }
  }
}
```

**Find everyone who acted in this movie**

```graphql
query {
  allMovies(where: { originalTitle_like: "%Infinity War%" }) {
    originalTitle
    filterCrews(where: { category_in: ["actor", "actress"] }) {
      category
      person {
        name
      }
    }
  }
}
```

**Find all movies Robert Downey Jr.(nm0000375) has acted in**

```graphql
query {
  aPerson(where: { id_is: "nm0000375" }) {
    casted {
      movies {
        originalTitle
      }
    }
  }
}
```

**Find all movies rated above 7**

**highest rated movie for 2019**

--filter relationships--

**Find all composers and the movies they worked on and their ratings**

```graphql
query {
  allCrews(where: { category_is: "composer" }) {
    movies {
      originalTitle
      rating {
        rating
      }
    }
  }
}
```

--many to many relationships--

**Find other actors that acted in the highest rated movie kevin bacon acted in**
