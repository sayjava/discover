import { buildSimpleSchema } from '../utils'
import { printSchema } from 'graphql'

test('that it creates a filter input for a type', () => {
  const schemaString = `
        type User @source(from: "users", name: "sqlite" ) {
            id: ID @unique
            name: String @field(name: "userName")
            stars: Int
            active: Boolean

            tasks: [Task!] @related(as: "owner")
        }
        
        
        type Task {
            id: ID
            taskName: String

            owner: User @related(by: "user_id")
        }
    `

  const schema = printSchema(buildSimpleSchema(schemaString))

  expect(schema).toMatchInlineSnapshot(`
    "directive @unique on FIELD_DEFINITION

    directive @field(
      \\"\\"\\"Remap this field to a different field in the source data\\"\\"\\"
      name: String
    ) on FIELD_DEFINITION

    directive @related(
      \\"\\"\\"
      The name of the field in the child's source data that should be used to connect this relatedship
      \\"\\"\\"
      by: String

      \\"\\"\\"
      The name of the field in the child's source data that should be used to connect this relatedship
      \\"\\"\\"
      as: String
    ) on FIELD_DEFINITION

    directive @source(
      \\"\\"\\"The name that this type should referred to in the source\\"\\"\\"
      name: String

      \\"\\"\\"The data source that this type should be source from\\"\\"\\"
      from: String
    ) on OBJECT

    \\"\\"\\"The response after creating a Task\\"\\"\\"
    type _TaskResponse {
      \\"\\"\\"The number of Tasks changed\\"\\"\\"
      totalRecordsAffected: Int!
      tasks: [Task!]!
    }

    \\"\\"\\"The response after creating a User\\"\\"\\"
    type _UserResponse {
      \\"\\"\\"The number of Users changed\\"\\"\\"
      totalRecordsAffected: Int!
      users: [User!]!
    }

    type Mutation {
      \\"\\"\\"Create a new User\\"\\"\\"
      create_users(user_objects: [NewUser!]): _UserResponse

      \\"\\"\\"Delete Users\\"\\"\\"
      delete_users(where: UserFilter!): _UserResponse

      \\"\\"\\"Delete Users\\"\\"\\"
      update_user(where: UserFilter!, set: NewUser!): _UserResponse

      \\"\\"\\"Create a new Task\\"\\"\\"
      create_tasks(task_objects: [NewTask!]): _TaskResponse

      \\"\\"\\"Delete Tasks\\"\\"\\"
      delete_tasks(where: TaskFilter!): _TaskResponse

      \\"\\"\\"Delete Tasks\\"\\"\\"
      update_task(where: TaskFilter!, set: NewTask!): _TaskResponse
    }

    \\"\\"\\"Create a new Task\\"\\"\\"
    input NewTask {
      id: ID
      taskName: String

      \\"\\"\\"Include User in the creation\\"\\"\\"
      owner: NewUser
    }

    \\"\\"\\"Create a new User\\"\\"\\"
    input NewUser {
      id: ID
      name: String
      stars: Int
      active: Boolean

      \\"\\"\\"Include Task in the creation\\"\\"\\"
      tasks: NewTask
    }

    type Query {
      find_users(where: UserFilter, orderBy: UserOrderBy, skip: Int, limit: Int): [User]
      find_tasks(where: TaskFilter, orderBy: TaskOrderBy, skip: Int, limit: Int): [Task]
    }

    type Task {
      id: ID
      taskName: String
      owner: User
    }

    input TaskFilter {
      id_is: ID
      id_is_not: ID
      id_like: ID
      id_in: [ID!]
      id_not_in: [ID!]
      taskName_is: String
      taskName_is_not: String
      taskName_like: String
      taskName_in: [String!]
      taskName_not_in: [String!]
      and: [TaskFilter!]
      or: [TaskFilter!]
    }

    enum TaskOrderBy {
      id_DESC
      id_ASC
      taskName_DESC
      taskName_ASC
    }

    type User {
      id: ID
      name: String
      stars: Int
      active: Boolean
      tasks: [Task!]
      filterTasks(where: TaskFilter, orderBy: TaskOrderBy, skip: Int, limit: Int): [Task!]
    }

    input UserFilter {
      id_is: ID
      id_is_not: ID
      id_like: ID
      id_in: [ID!]
      id_not_in: [ID!]
      name_is: String
      name_is_not: String
      name_like: String
      name_in: [String!]
      name_not_in: [String!]
      stars_is: Int
      stars_is_not: Int
      stars_lte: Int
      stars_lt: Int
      stars_gte: Int
      stars_gt: Int
      active_is: Boolean
      active_is_not: Boolean
      and: [UserFilter!]
      or: [UserFilter!]
    }

    enum UserOrderBy {
      id_DESC
      id_ASC
      name_DESC
      name_ASC
      stars_DESC
      stars_ASC
      active_DESC
      active_ASC
    }
    "
  `)
})
