import { parse, buildASTSchema } from 'graphql'
import generateDefinitions from '../lib/defintiions/generate'

export const buildSimpleSchema = (schema: string) => {
  const document = parse(schema)

  Object.assign(document, {
    definitions: generateDefinitions(document.definitions.concat()),
  })

  return buildASTSchema(document)
}
