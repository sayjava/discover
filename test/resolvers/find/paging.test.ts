import { graphql } from 'graphql'
import { APIConfig, Datasource } from '../../../index'
import { createAPI } from '../../../lib'

const simpleSchema = `
    type User @source(from: "sql") {
        id:ID
        name:String
        tasks: [Task!]
    }

    type Task @source(from: "sql") {
        id: ID
        name: String
    }
`

test('that it creates paging for top level field', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(skip: 2, limit: 10) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "limit": 10,
      "skip": 2,
    }
  `)
})

test('that it creates paging second level fields', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users {
        name
        filterTasks(limit: 2, skip: 4) {
            name
        }
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.relations.filterTasks.model.criteria).toMatchInlineSnapshot(`
    Object {
      "limit": 2,
      "skip": 4,
    }
  `)
})
