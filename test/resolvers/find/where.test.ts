import { graphql } from 'graphql'
import { APIConfig, Datasource } from '../../../index'
import { createAPI } from '../../../lib'

const simpleSchema = `
    type User @source(from: "sql") {
        id:ID
        name:String
        tasks: [Task!]
    }

    type Task @source(from: "sql") {
        id: ID
        name: String
    }
`

test('that it creates an equal where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(where:{name_is: "funny user"}) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "and": Array [
        Object {
          "field": "name",
          "type": "EQUAL_TO",
          "value": "funny user",
        },
      ],
    }
  `)
})

test('that it creates a like where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(where:{name_like: "%funny%"}) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "and": Array [
        Object {
          "field": "name",
          "type": "LIKE",
          "value": "%funny%",
        },
      ],
    }
  `)
})

test('that it creates a not in where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(where:{name_not_in: ["007", "bond", "james"]}) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "and": Array [
        Object {
          "field": "name",
          "type": "NOT_IN",
          "value": Array [
            "007",
            "bond",
            "james",
          ],
        },
      ],
    }
  `)
})

test('that it creates a in where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(where:{name_in: ["007", "bond", "james"]}) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "and": Array [
        Object {
          "field": "name",
          "type": "IN",
          "value": Array [
            "007",
            "bond",
            "james",
          ],
        },
      ],
    }
  `)
})

test('that it creates an and where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(where: {
        and: [{name_is: "bond"}, {name_is: "james"}]
      }  ) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "and": Array [
        Object {
          "field": "name",
          "type": "EQUAL_TO",
          "value": "bond",
        },
        Object {
          "field": "name",
          "type": "EQUAL_TO",
          "value": "james",
        },
      ],
    }
  `)
})

test('that it creates an or where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(where: {
        or: [{name_is: "bond"}, {name_is: "james"}]
      }  ) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "or": Array [
        Object {
          "field": "name",
          "type": "EQUAL_TO",
          "value": "bond",
        },
        Object {
          "field": "name",
          "type": "EQUAL_TO",
          "value": "james",
        },
      ],
    }
  `)
})

test('that it creates a not_equal where clause', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users {
        name
        filterTasks(where: {name_is_not: "james bond"}) {
            name
        }
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.relations.filterTasks.model.criteria).toMatchInlineSnapshot(`
    Object {
      "and": Array [
        Object {
          "field": "name",
          "type": "NOT_EQUAL_TO",
          "value": "james bond",
        },
      ],
    }
  `)
})
