import { graphql } from 'graphql'
import { APIConfig, Datasource } from '../../../index'
import { createAPI } from '../../../lib'

const simpleSchema = `
    type User @source(from: "sql") {
        id:ID
        name:String
        tasks: [Task!]
    }

    type Task @source(from: "sql") {
        id: ID
        name: String
    }
`

test('that it creates paging for top level field', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users(orderBy: name_DESC) {
        name
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.criteria).toMatchInlineSnapshot(`
    Object {
      "orderBy": Object {
        "direction": "DESC",
        "field": "name",
      },
    }
  `)
})

test('that it creates paging second level fields', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users {
        name
        filterTasks(orderBy: name_DESC) {
            name
        }
      }
    }`

  const schema = createAPI(config)
  const result = await graphql(schema, query)
  const [[call]] = (<jest.Mock>defaultSource.find).mock.calls

  expect(result.errors).toBeUndefined()
  expect(call.relations.filterTasks.model.criteria).toMatchInlineSnapshot(`
    Object {
      "orderBy": Object {
        "direction": "DESC",
        "field": "name",
      },
    }
  `)
})
