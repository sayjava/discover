import { buildSchema, ObjectTypeDefinitionNode, parse } from 'graphql'
import createFindResolver from '../../../lib/resolvers/find/create'
import generate from '../../../lib/models/generate'

test('that it creates a resolver for each defined definition', () => {
  const simpleSchema = `
    type User {
        id:ID
        name:String
    }
`
  const defintions = parse(simpleSchema).definitions.concat()
  const models = generate(defintions)

  const userType = <ObjectTypeDefinitionNode>(
    buildSchema(simpleSchema).getType('User').astNode
  )

  const resolverDef = {
    definition: userType,
    models,
    sources: {
      sql: {
        find: jest.fn(() => Promise.resolve([])),
      },
    },
  }

  const resolvers = createFindResolver(resolverDef)

  expect(resolvers).toMatchInlineSnapshot(`
    Object {
      "Query": Object {
        "find_users": [Function],
      },
      "User": Object {},
    }
  `)
})

test('that it creates a resolver for dependent types with a different source', () => {
  const simpleSchema = `

    directive @source(name: String, from: String) on OBJECT

    type User @source(from: "sql") {
        id:ID
        name:String

        tasks: [Task!]
        avatar: Avatar
    }

    type Task @source(from: "sql") {
        id: ID
        name: String
    }

    type Avatar @source(from: "facbook") {
      id: ID
      url: String
    }
`

  const defintions = parse(simpleSchema).definitions.concat()
  const models = generate(defintions)

  const userType = <ObjectTypeDefinitionNode>(
    buildSchema(simpleSchema).getType('User').astNode
  )

  const resolverDef = {
    definition: userType,
    models,
    sources: {
      sql: {
        find: jest.fn(() => Promise.resolve([])),
      },
    },
  }

  const resolvers = createFindResolver(resolverDef)

  expect(resolvers).toMatchInlineSnapshot(`
    Object {
      "Query": Object {
        "find_users": [Function],
      },
      "User": Object {
        "avatar": [Function],
      },
    }
  `)
})
