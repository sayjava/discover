import { graphql } from 'graphql'
import { APIConfig, Datasource } from '../../../index'
import { createAPI } from '../../../lib'

const simpleSchema = `
    type User @source(from: "sql") {
        id:ID
        name:String

        tasks: [Task!]
    }

    type Task @source(from: "sql") {
        id: ID
        name: String

        attachment: Attachment!
    }

    type Attachment @source(from: "drive") {
        id: ID
        file: String
    }
`

test('that it calls the find method of the datasource', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users {
        name
      }
    }
  `

  const schema = createAPI(config)
  const result = await graphql(schema, query)

  expect(result.errors).toBeUndefined()
  expect(defaultSource.find).toHaveBeenCalledWith({
    attributes: {
      name: {
        name: 'name',
        sourceName: 'name',
        type: 'string',
        unique: false,
      },
    },
    criteria: {},
    identities: [
      {
        name: 'id',
        sourceName: 'id',
        type: 'ID',
        unique: true,
      },
    ],
    name: 'User',
    relations: {},
    source: {
      from: 'sql',
      name: 'User',
    },
  })
})

test('that it calls the find method of the datasource with relationships', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users {
        name

        filterTasks {
            name
        }
      }
    }
  `

  const schema = createAPI(config)
  const result = await graphql(schema, query)

  expect(result.errors).toBeUndefined()
  expect(defaultSource.find).toHaveBeenCalledWith({
    attributes: {
      name: {
        name: 'name',
        sourceName: 'name',
        type: 'string',
        unique: false,
      },
    },
    criteria: {},
    identities: [
      {
        name: 'id',
        sourceName: 'id',
        type: 'ID',
        unique: true,
      },
    ],
    name: 'User',
    relations: {
      filterTasks: {
        condition: {
          by: 'id',
        },
        isCollection: true,
        model: {
          attributes: {
            name: {
              name: 'name',
              sourceName: 'name',
              type: 'string',
              unique: false,
            },
          },
          criteria: {},
          identities: [
            {
              name: 'id',
              sourceName: 'id',
              type: 'ID',
              unique: true,
            },
          ],
          name: 'Task',
          relations: {},
          source: {
            from: 'sql',
            name: 'Task',
          },
        },
        type: 'Task',
      },
    },
    source: {
      from: 'sql',
      name: 'User',
    },
  })
})

test('that different types are sourced from their stated sources', async () => {
  let defaultModel: any
  let attachmentModel: any

  const defaultSource: Datasource = {
    find: jest.fn(arg => {
      defaultModel = arg

      return Promise.resolve([
        {
          name: 'test_user',
          filterTasks: [
            {
              name: 'sample_task',
            },
          ],
        },
      ])
    }),
  }

  const attachmentSource: Datasource = {
    find: jest.fn(arg => {
      attachmentModel = arg

      return Promise.resolve([
        {
          name: 'test_attachment',
        },
      ])
    }),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
      drive: attachmentSource,
    },
  }

  const query = `
    query {
      find_users {
        name
        filterTasks {
            name
            attachment {
                file
            }
        }
      }
    }
  `

  const schema = createAPI(config)
  const result = await graphql(schema, query)

  expect(result.errors).toBeUndefined()

  expect(defaultModel.relations).toMatchInlineSnapshot(`
    Object {
      "filterTasks": Object {
        "condition": Object {
          "by": "id",
        },
        "isCollection": true,
        "model": Object {
          "attributes": Object {
            "name": Object {
              "name": "name",
              "sourceName": "name",
              "type": "string",
              "unique": false,
            },
          },
          "criteria": Object {},
          "identities": Array [
            Object {
              "name": "id",
              "sourceName": "id",
              "type": "ID",
              "unique": true,
            },
          ],
          "name": "Task",
          "relations": Object {},
          "source": Object {
            "from": "sql",
            "name": "Task",
          },
        },
        "type": "Task",
      },
    }
  `)

  expect(attachmentModel).toMatchInlineSnapshot(`
    Object {
      "attributes": Object {
        "file": Object {
          "name": "file",
          "sourceName": "file",
          "type": "string",
          "unique": false,
        },
      },
      "criteria": Object {},
      "identities": Array [
        Object {
          "name": "id",
          "sourceName": "id",
          "type": "ID",
          "unique": true,
        },
      ],
      "name": "Attachment",
      "relations": Object {},
      "source": Object {
        "from": "drive",
        "name": "Attachment",
      },
    }
  `)
})
