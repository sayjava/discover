import { buildSchema, ObjectTypeDefinitionNode } from 'graphql'
import createResolver from '../../lib/resolvers/create'

test('that it creates a resolver for each defined definition', () => {
  const simpleSchema = `
    type User {
        id:ID
        name:String
    }
`
  const userType = buildSchema(simpleSchema).getType('User')
  const resolvers = createResolver(userType.astNode as ObjectTypeDefinitionNode)

  expect(resolvers).toMatchInlineSnapshot(`
    Object {
      "Mutation": Object {
        "create_users": [Function],
      },
    }
  `)
})
