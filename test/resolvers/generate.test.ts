import { graphql } from 'graphql'
import { APIConfig, Datasource } from '../../index'
import { createAPI } from '../../lib'

const simpleSchema = `
    type User @source(from: "sql") {
        id:ID
        name:String

        tasks: [Task!]
    }

    type Task @source(from: "jira") {
        id: ID
        name: String
    }
`

test('that it create resolvers from all defined types', async () => {
  const defaultSource: Datasource = {
    find: jest.fn(() => Promise.resolve([])),
  }

  const config: APIConfig = {
    schema: simpleSchema,
    sources: {
      sql: defaultSource,
    },
  }

  const query = `
    query {
      find_users {
        name
      }
    }
  `

  const schema = createAPI(config)
  const result = await graphql(schema, query)

  expect(result.errors).toBeUndefined()
  expect(defaultSource.find).toHaveBeenCalled()
})
