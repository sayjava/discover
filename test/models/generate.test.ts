import { parse } from 'graphql'
import discoverSchema from '../../lib/models/generate'

const sampleSchema = `
    
    directive @source( name: String, from: String) on OBJECT

    type User {
        id: ID
        name: String
    }

    type Task @source(name: "tasks", from: "sqlite" ) {
        id: ID
        title: String
    }

    type Query {
        hello: String
    }
`

const schema = parse(sampleSchema)

test('that it discovers all relevant models in the schema', () => {
  const models = discoverSchema(schema.definitions.concat())

  expect(Object.values(models).map(m => m.name)).toMatchInlineSnapshot(`
    Array [
      "User",
      "Task",
    ]
  `)
})

test('that it creates the sources for discovered models', () => {
  const models = discoverSchema(schema.definitions.concat())

  expect(Object.values(models).map(m => m.source)).toMatchInlineSnapshot(`
    Array [
      Object {
        "from": "default",
        "name": "User",
      },
      Object {
        "from": "sqlite",
        "name": "tasks",
      },
    ]
  `)
})
