import { parse } from 'graphql'
import discoverSchema from '../../lib/models/generate'

const sampleSchema = `
    
    directive @source( name: String, from: String) on OBJECT
    directive @field(name: String) on FIELD_DEFINITION

    type User {
        id: ID
        name: String
        
        tasks: [Task!]
    }

    type Task @source(name: "tasks", from: "sqlite" ) {
        id: ID @field(name: "task_id")
        title: String
    }

    type Query {
        hello: String
    }
`

const schema = parse(sampleSchema)

test('that it creates attributes on model types', () => {
  const models = discoverSchema(schema.definitions.concat())

  expect(models['Task'].attributes).toMatchInlineSnapshot(`
    Object {
      "id": Object {
        "name": "id",
        "sourceName": "task_id",
        "type": "ID",
        "unique": true,
      },
      "title": Object {
        "name": "title",
        "sourceName": "title",
        "type": "string",
        "unique": false,
      },
    }
  `)
})
