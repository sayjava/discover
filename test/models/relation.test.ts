import { parse } from 'graphql'
import discoverSchema from '../../lib/models/generate'

const sampleSchema = `
    
    directive @source( name: String, from: String) on OBJECT
    directive @related( by: String, as: String) on FIELD_DEFINITION

    type User {
        id: ID
        name: String

        tasks: [Task!] @related(as: "owner")
    }

    type Task @source(name: "tasks", from: "sqlite" ) {
        id: ID
        title: String

        owner: User! @related(by: "userId")
    }

    type Query {
        hello: String
    }
`

const schema = parse(sampleSchema)

test('that it discovers the relationships between the user and task', () => {
  const models = discoverSchema(schema.definitions.concat())

  const userRelation = models['User'].relations

  expect(Object.keys(userRelation)).toMatchInlineSnapshot(`
    Array [
      "tasks",
    ]
  `)

  expect(userRelation['tasks'].condition).toMatchInlineSnapshot(`
    Object {
      "as": "owner",
      "by": "id",
    }
  `)
  expect(userRelation['tasks'].isCollection).toBeTruthy()
})
